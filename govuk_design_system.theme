<?php

/**
 * @file
 * Functions to support theming in the govuk_design_system theme.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements hook_theme().
 */
function govuk_design_system_theme() {
  return [
    'govuk_design_system_phase_banner' => [
      'variables' => [
        'phase' => '',
        'message' => '',
      ],
      'template' => 'components/phase-banner',
    ],
  ];
}

/**
 * Implements hook_theme_registry_alter().
 */
function govuk_design_system_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
  $elements_to_vary_by_type = [
    'form_element',
  ];

  // Add theme suggestions for form_element__type.
  if (in_array($hook, $elements_to_vary_by_type) && isset($variables['element']['#type'])) {
    $suggestions[] = $hook . '__' . $variables['element']['#type'];
  }

}

/**
 * Implements hook_preprocess_html().
 *
 * Add "Error: " to the page title.
 *
 * @See https://design-system.service.gov.uk/components/error-summary/#how-it-works
 */
function govuk_design_system_preprocess_html(&$variables) {
  $errors = \Drupal::messenger()->messagesByType('error');
  if (!empty($errors)) {
    $variables['head_title']['title'] = t('Error:') . ' ' . $variables['head_title']['title'];
  }
}
/**
 * Implements hook_preprocess_html().
 */
function govuk_design_system_preprocess_page(&$variables) {
  if ($phase = theme_get_setting('phase_banner')) {
    $variables['phase_banner'] = [
      '#theme' => 'govuk_design_system_phase_banner',
      '#phase' => $phase,
      '#message' => theme_get_setting('phase_banner_message')['value'],
    ];
  }
}

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function govuk_design_system_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['govuk_design_system'] = [
    '#type' => 'fieldset',
    '#title' => t('GOVUK Design System'),
  ];

  $phase_banner_url = Url::fromUri('https://design-system.service.gov.uk/components/phase-banner/');
  $phase_banner_link = Link::fromTextAndUrl(t('phase banner'), $phase_banner_url);
  $form['govuk_design_system']['phase_banner'] = [
    '#type' => 'select',
    '#title' => t('Phase banner'),
    '#default_value' => theme_get_setting('phase_banner'),
    '#options' => [
      'demo' => t('Demo'),
      'alpha' => t('Alpha'),
      'beta' => t('Beta'),
    ],
    '#empty_option' => t('None'),
    '#description' => t('Select a @phase_banner to be displayed on your site.', ['@phase_banner' => $phase_banner_link->toString()]),
  ];

  // Workaround for https://www.drupal.org/node/997826
  $form['govuk_design_system']['phase_banner_message_container'] = [
    '#type' => 'container',
    '#states' => [
      'visible' => [
        ':input[name="phase_banner"]' => [
          ['value' => 'alpha'],
          ['value' => 'beta'],
        ],
      ],
    ],
  ];

  $setting = theme_get_setting('phase_banner_message');
  $form['govuk_design_system']['phase_banner_message_container']['phase_banner_message'] = [
    '#type' => 'text_format',
    '#title' => t('Phase banner message'),
    '#format' => $setting['format'],
    '#default_value' => $setting['value'],
  ];
}
